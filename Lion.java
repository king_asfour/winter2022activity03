public class Lion{
	public String diet;
	public double height;
	public int speed;
	
	public void goRun(){
		System.out.println("I just ran at a speed of " + speed + "km/h");
	}
	public void doRoar(){
		System.out.println("I just roar loudly!!!");
	}
}