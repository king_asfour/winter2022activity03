public class Application{
	public static void main(String[] args){
		Lion animalLion = new Lion();
		animalLion.diet = "Carnivorous";
		animalLion.height = 1.2;
		animalLion.speed = 80;
		
		Lion goat = new Lion();
		goat.diet = "Herbivore";
		goat.height = 0.58;
		goat.speed = 56;
		
		System.out.println(animalLion.diet);
		System.out.println(animalLion.height);
		System.out.println(animalLion.speed);
		
		animalLion.goRun();
		animalLion.doRoar();
		goat.goRun();
		goat.doRoar();
		
		Lion[] pride = new Lion[3];
		pride[0] = animalLion;
		pride[1] = goat;
		pride[2] = new Lion();
		System.out.println(pride[0].diet);
		System.out.println(pride[1].diet);
		System.out.println(pride[2].height);
		
		
		//Documentation for myself
		
		//Lion[] pride = new Lion[3];
		//pride[0] = new Lion();
		//pride[0].diet = "water";
		//pride[1] = goat;
		
		//for(int i = 0; i < pride.length;i++){
			//System.out.println(pride[i].diet);
		//}
	}
}